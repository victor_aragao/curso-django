from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Produto, Cliente
from django.http import HttpResponse
from django.template import loader


def index(request):
    produtos = Produto.objects.all()

    response = {
        'produtos': produtos
    }

    return render(request, 'index.html', response)


def produto(request, id):

    #prod = Produto.objects.get(id=id)
    prod = get_object_or_404(Produto, id=id)
    response = {
        'produto': prod
    }

    return render(request, 'produto.html', response)


def error404(request, ex):
    template = loader.get_template('404.html')
    return HttpResponse(content=template.render(), content_type='text/html; charset=utf8', status=404)


def error500(request, ex):
    template = loader.get_template('500.html')
    return HttpResponse(content=template.render(), content_type='text/html; charset=utf8', status=500)
